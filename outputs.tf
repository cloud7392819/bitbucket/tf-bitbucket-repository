# ws@2023 outputs.tf

##################################################################
# Repository
##################################################################

output "clone_https" {
  value = bitbucket_repository.default["0"].clone_https
}

output "clone_ssh" {
  value = bitbucket_repository.default["0"].clone_ssh
}

output "uuid" {
  value = bitbucket_repository.default["0"].uuid
}
