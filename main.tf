@ ws@2023 main.tf

##################################################################
# Branching model
##################################################################

resource "bitbucket_branching_model" "default" {

  for_each   = local.should_create_branching_model ? { 0 = 0 } : {}

  owner      = var.owner
  repository = bitbucket_repository.default["0"].name

  // This has idempotency issues with provider 2.20
  dynamic "development" {
    for_each = { 0 = var.branching_model_development }

    content {
      branch_does_not_exist = development.value.branch_does_not_exist
      use_mainbranch        = development.value.use_mainbranch
      name                  = development.value.name
    }
  }

  dynamic "production" {
    for_each = var.branching_model_production == null ? {} : { 0 = var.branching_model_production }

    content {
      enabled               = true
      branch_does_not_exist = production.value.branch_does_not_exist
      use_mainbranch        = production.value.use_mainbranch
      name                  = production.value.name
    }
  }
}

##################################################################
# Branch protection
##################################################################

resource "bitbucket_branch_restriction" "default" {

  for_each = local.all_branch_protections_rules

  owner      = var.owner
  repository = bitbucket_repository.default["0"].name

  kind    = split(",", each.key)[1]
  value   = each.value
  pattern = split(",", each.key)[0]
  users   = split(",", each.key)[1] == "push" ? var.branch_restriction_push_allowed_users : null
}


##################################################################
# Reviewers
##################################################################

resource "bitbucket_default_reviewers" "default" {

  owner      = var.owner
  repository = bitbucket_repository.default["0"].name

  reviewers = var.reviewers

  depends_on = [
    bitbucket_repository_group_permission.this
  ]
}

##################################################################
# Group Permissions
##################################################################

resource "bitbucket_repository_group_permission" "default" {

  for_each = var.group_permissions == null ? {} : var.group_permissions

  workspace  = var.owner
  repo_slug  = local.slug
  group_slug = each.key
  permission = each.value.permission

  depends_on = [
    bitbucket_repository.this
  ]
}

##################################################################
# Webhooks
##################################################################

resource "bitbucket_hook" "default" {

  for_each = var.hooks != null ? var.hooks : {}

  owner      = var.owner
  repository = bitbucket_repository.default["0"].name

  url         = each.value["url"]
  description = each.value["description"]
  events      = each.value["events"]
}


