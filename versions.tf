# ws@2023 varsions.tf

terraform {
  required_version = ">= 1.3"

  required_providers {
    bitbucket = {
      source  = "DrFaust92/bitbucket"
      version = ">= 2.30"
    }
  }
}

